
---- MODULE AudioChannel ----

EXTENDS
  Naturals

CONSTANTS
  MAX_BLOBS

VARIABLES
  channels,
  nextBlob

\***************************************

vars == <<channels, nextBlob>>

Other(i) ==
  IF i = 1 THEN 2 ELSE 1

Load(i) ==
  /\  nextBlob = channels[i].blob
  /\  nextBlob <= MAX_BLOBS
  /\  channels[i].state \in {"idle", "complete"}
  /\  channels' = [channels EXCEPT
        ![i].state = "loading",
        ![i].blob = nextBlob
      ]
  /\  nextBlob' = nextBlob + 1

Ready(i) ==
  /\  channels[i].state = "loading"
  /\  channels' = [channels EXCEPT ![i].state = "ready"]
  /\  UNCHANGED <<nextBlob>>

Play(i) ==
  /\  channels[i].state = "ready"
  /\  channels[Other(i)].state /= "playing"
  /\  channels' = [channels EXCEPT ![i].state = "playing"]
  /\  UNCHANGED <<nextBlob>>

Complete(i) ==
  /\  channels[i].state = "playing"
  /\  channels' = [channels EXCEPT ![i].state = "complete"]
  /\  UNCHANGED <<nextBlob>>

Nudge(i) ==
  /\  channels[i].state \in {"ready", "playing"}
  /\  channels[Other(i)].state \in {"idle", "complete"}
  /\  channels' = [channels EXCEPT
        ![Other(i)].blob = channels[i].blob + 1
      ]
  /\  UNCHANGED <<nextBlob>>

Playback(i) ==
  \/  Load(i)
  \/  Ready(i)
  \/  Play(i)
  \/  Complete(i)

----

Init ==
  /\  channels = <<
        [id |-> 1, state |-> "idle", blob |-> 1],
        [id |-> 2, state |-> "idle", blob |-> 0]
      >>
  /\  nextBlob = 1

Next ==
  \E i \in {1, 2} :
    \/  Playback(i)
    \/  Nudge(i)

Liveness ==
  WF_vars(Next)

Spec ==
  /\  Init
  /\  [][Next]_vars
  /\  Liveness

----

TypeOK ==
  /\  \A i \in {1, 2} :
        channels[i] \in [
          id : {1, 2},
          state : {"idle", "loading", "ready", "playing", "complete"},
          blob : {0} \cup 1 .. MAX_BLOBS + 1
        ]
  /\  nextBlob <= MAX_BLOBS + 1

----

\* Channels are exactly one step ahead or behind each other
OneStepAheadOrBehind ==
  [] (\A i \in {1, 2} :
    \/  channels[i].blob = channels[Other(i)].blob + 1
    \/  channels[i].blob = channels[Other(i)].blob - 1)

\* At any given time, only one of the channels is playing
ExclusivePlay ==
  [] \A i \in {1, 2} :
    channels[i].state = "playing" => channels[Other(i)].state /= "playing"

\* When all blobs are consumed, both channels move to complete eventually
PlaybackSettles ==
  nextBlob >= MAX_BLOBS ~>
    \A i \in {1, 2} :
      channels[i].state = "complete"

\* Two channels are simultaneously active often enough
ActiveOverlap ==
  LET
    activeStates == {"loading", "ready", "playing"}
  IN
    []<> (\A i \in {1, 2} :
      /\  channels[i].state \in activeStates
      /\  channels[Other(i)].state \in activeStates)

\* Channels gets to playback frequently
ChannelsPlayOften ==
  []<> <<\E i \in {1, 2} : Play(i)>>_vars

====
