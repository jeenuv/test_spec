
---- MODULE Test ----

EXTENDS Naturals, TLC

CONSTANTS
  total

VARIABLES
  done,
  fillQty,
  fills,
  nextFillNumber

vars == <<
    done,
    fillQty,
    fills,
    nextFillNumber
  >>

InsertFill ==
  LET
    remaining == total - fillQty
  IN
    /\  remaining > 0
    /\  \E f \in 1 .. remaining :
          /\  fills' = fills \cup {<<nextFillNumber, f>>}
          /\  fillQty' = fillQty + f
          /\  nextFillNumber' = nextFillNumber + 1
          /\  UNCHANGED <<done>>

Finish ==
  /\  done = FALSE
  /\  fillQty = total
  /\  done' = TRUE
  /\  UNCHANGED <<fills, fillQty, nextFillNumber>>

(*********** Specification ***********)

Init ==
  /\  done = FALSE
  /\  fillQty = 0
  /\  fills = {}
  /\  nextFillNumber = 0

Next ==
  \/  InsertFill
  \/  Finish

Spec ==
  /\  Init
  /\  [][Next]_vars

(*********** Properties ***********)

Complete ==
  [] (done = TRUE => fillQty = total)

====
